package src.ru.bildovich;

import java.sql.*;
import java.util.Scanner;

/**
 * Created by mac on 28.07.17.
 * Class
 */
public class Program {

    public static final String driverClass = "com.mysql.jdbc.Driver";
    public static final String connString = "jdbc:mysql://127.0.0.1:3306/new_schema?user=bildovich&password=1";
    private static Connection conn;
    private static Statement cmd;
    private static ResultSet result;



    //"/Users/mac/Desktop/projects/mysql-connector-java-5.1.43-bin.jar"
    public static void main(String[] args) throws SQLException {

        try {

            Class.forName(driverClass);
            conn = DriverManager.getConnection(connString);

            CallableStatement sp = conn.prepareCall("call getCount(?)");
            sp.execute();
            int count = sp.getInt(1);
            System.out.printf("Всего авторов: %d\n", count);


            System.out.print("Поиск: ");
            Scanner sc = new Scanner(System.in);
            String search = sc.nextLine().trim();
            sc.close();

            sp = conn.prepareCall("call getAuthorCounterByTitle(?, ?)");
            sp.setString(1, "%" + search + "%");

            sp.execute();
            count = sp.getInt(2);

            System.out.printf("Найдено авторов: %d/n", count);

//            String sql = "INSERT INTO author (fio, birthday) VALUE (?, ?)";
//            PreparedStatement cmd = conn.prepareStatement(sql);
//
//            cmd.setString(1, fio);
//            cmd.setString(2, "1954-10-29");
//
//            if (cmd.executeUpdate() == 1) {
//                System.out.println("Курс добавлен");
//            }


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
//            result.close();
            conn.close();
        }


//
//        try {
////            while (result.next()) {
////
////                String fio = result.getString("name");
////
////                System.out.println(fio);
////
//////                Date d = result.getDate("birthday");
//////                System.out.println(d);
//
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            result.close();
//        }




    }
}
