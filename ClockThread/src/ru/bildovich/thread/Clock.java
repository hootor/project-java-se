package ru.bildovich.thread;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by mac on 25.09.17.
 * Class
 */
public class Clock extends JFrame {

    private static final String START = "Start";
    private static final String STOP = "Stop";
    private JLabel m_time;

    public Clock() {

        setTitle("Clock thread");

        m_time = new JLabel();

        m_time.setHorizontalAlignment(SwingConstants.CENTER);

        Font f = new Font("Default", Font.BOLD + Font.ITALIC, 24);

        m_time.setFont(f);

        getContentPane().add(m_time);

        setBounds(0, 0, 300, 100);

        Thread m_thr = new MyThread(this);
        m_thr.start();
    }

    public void setTime() {
//        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
//        m_time.setText(df.format(Calendar.getInstance().getTime()));

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                m_time.setText(df.format(Calendar.getInstance().getTime()));
            }
        });
    }

    public void actionPerfomed(ActionEvent e) {

//        if (e.getActionCommand().equals(START)) {
//            timeThread = new MyThread(this);
//            new Thread(timThread).start();
//        }
//
//        if (e.getActionCommand().equals(STOP)) {
//
//        }
    }
}



class MyThread extends Thread{

    private Clock clock;

    public MyThread(Clock clock) {
        this.clock = clock;
    }

    @Override
    public void run() {
        while (true) {
            clock.setTime();
            try {
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}