import java.util.Arrays;

/**
 * Created by mac on 11.02.17.
 */
public class Main {
    public static void main(String[] args) {

//        System.out.println(contains("но как говориться ", " г"));


        int[] a = {1, 1, 1, 102, 111, 1000};
        int[] b = {1, 1, 1, 9, 2000};
        System.out.println(Arrays.toString(contTwoMass(a, b)));

    }

    public static boolean contains(String origin, String sub){
        if (sub.length() > origin.length()) return false;
        char[] origArr = origin.toCharArray();
        char[] subArr = sub.toCharArray();
        for (int i = 0; i <= (origArr.length - subArr.length); i++) {
            for (int j = 0; j < subArr.length; j++) {
                if (origArr[i + j] != subArr[j]) break;
                if (j == subArr.length - 1) return true;
            }
        }
        return false;
    }

    public static int[] contTwoMass(int[] a, int[] b) {

        int n = a.length;
        int m = b.length;

        int[] result = new int[n + m];

        int i = 0, j = 0;

        int index = 0;

        while (i < n && j < m) {

            if (a[i] < b[j]) {
                result[index] = a[i];
                i++;
            } else {
                result[index] = b[j];
                j++;
            }

            index++;
        }

        while (i < n) {
            result[index] = a[i];
            index++;
            i++;
        }

        while (j < m) {
            result[index] = b[j];
            index++;
            j++;
        }

        return result;

//        int[] c = new int[a.length + b.length];
//
//        int length = Math.max(a.length, b.length);
//
//        for (int i = 0; i < length; i++) {
//
//            if ((i + 1)>a.length || (i + 1)>b.length){
//
//                if ((i + 1)>a.length && (i + 1)>b.length){
//
//                } else {
//                    if ((i + 1)>a.length){
//
//                    } else {
//
//                    }
//                }
//
//            } else {
//
//            }
//
//        }

//        // объединение двух массивов в один
//        for (int i = 0; i < c.length / 2; i++) {
//            if (a[i] > b[i]) {
//                c[i + i] = b[i];
//                c[i + i + 1] = a[i];
//            } else {
//                c[i + i] = a[i];
//                c[i + i + 1] = b[i];
//
//            }
//
//        }
//
//        // проверка упорядоченности массива, в случае, если
//        // элемент одного массива меньше предыдущего элемента
//        // второго массива
//
//        for (int i = 0; i < c.length - 1; i++) {
//            int t = c[i];
//            if (c[i] > c[i + 1]) {
//                c[i] = c[i + 1];
//                c[i + 1] = t;
//            }
//
//        }
//
//        return c;
    }

}
