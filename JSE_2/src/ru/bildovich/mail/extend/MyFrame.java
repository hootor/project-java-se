package ru.bildovich.mail.extend;

import oracle.jvm.hotspot.jfr.JFR;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mac on 02.09.17.
 * Class
 */
public class MyFrame extends JFrame {

//    @Override
//    public void setTitle(String title) {
//        title = "TITLE: " + title;
//        super.setTitle(title);
//    }

    public void initOvals() {

        GridLayout gl = new GridLayout(2, 3);
        setLayout(gl);

        for (int i = 0; i < 6; i++) {
            RectComponent oc = new RectComponent();
            oc.setGap(10);
            add(oc);
        }

    }
}
