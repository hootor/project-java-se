package ru.bildovich.mail.extend;

import javax.swing.*;

/**
 * Created by mac on 02.09.17.
 * Class
 */
public class Starter {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.initOvals();
        frame.setBounds(200, 200, 300, 300);
        frame.setVisible(true);
    }
}
