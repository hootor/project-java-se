package ru.bildovich.mail.extend;

import java.awt.*;

/**
 * Created by mac on 02.09.17.
 * Class
 */
public class RectComponent extends OvalComponent {

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawOval(getGap(), getGap(), getWidth() - 2 * getGap(), getHeight() - 2 * getGap());
    }
}
