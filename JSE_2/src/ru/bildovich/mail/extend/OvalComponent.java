package ru.bildovich.mail.extend;

import javax.swing.*;
import java.awt.*;

/**
 * Created by mac on 02.09.17.
 * Class
 */
public class OvalComponent extends JComponent {

    private int gap;

    public int getGap() {
        return gap;
    }

    public void setGap(int gap) {
        this.gap = gap;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawRect(gap, gap, getWidth() - 2 * gap, getHeight() - 2 * gap);
//        g.drawOval(gap, gap, getWidth() - 2 * gap, getHeight() - 2 * gap);
    }


}
