package ru.bildovich;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mac on 11.09.17.
 * Class
 */
public class ManagerFrame extends JFrame implements ActionListener {

    private static final String UP = "UP";
    private static final String DN = "DN";

    private static final int STEP = 10;

    private JFrame frame;

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public ManagerFrame() {

        SimpleClass sc = new SimpleClass();

        JButton up = new JButton("UP");
        up.setActionCommand(UP + "_C");
        up.addActionListener(this);
        add(up, BorderLayout.NORTH);

        JButton dn = new JButton("DOWN");
        dn.setActionCommand(DN + "_C");
        dn.addActionListener(this);
        add(dn, BorderLayout.SOUTH);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(200, 200, 200, 200);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Rectangle r = frame.getBounds();
        if ((UP + "_C").equals(e.getActionCommand())) {
//            System.out.println(UP);
            r.y -= STEP;
        } else if ((DN + "_C").equals(e.getActionCommand())) {
//            System.out.println(DN);
            r.y += STEP;
        }
        frame.setBounds(r);
    }
}

class SimpleClass implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Button is pressed");
    }
}
