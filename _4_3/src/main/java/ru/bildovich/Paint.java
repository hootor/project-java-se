package ru.bildovich;

/**
 * Created by mac on 04.02.17.
 */
public class Paint {
    String piramid(int h){
        int center = h/2;
        StringBuilder sb = new StringBuilder();

        h=h*2;
        if(h%2 == 0) h++;
        for(int i=0; i<h; i+=2)
        {
            //Печатаем необходимое количество пробелов
            //(size-1)/2 - т.к. size всегда нечетное, то вычитаем 1,
            //делим пополам чтобы выйти на центр строки,
            //вычитаем i/2, т.к. i увеличивается, то количество пробелов уменьшается,
            //а делим на 2, т.к. шаг i = 2, а количество пробелов должно
            //уменьшаться на 1
            for(int j=0; j<(h-1)/2-(i/2); j++) sb.append("#");
            //Печатаем необходимое количество *
            //i+1, т.к. цикл i начинается с нуля, а в первой строке должна быть одна *
            //для упрощения этого цикла можно было бы i начать с 1, но тогда
            //усложнится условие предыдущего цикла
            for(int j=0; j<i+1; j++) sb.append('*');
            //Перевод строки
            sb.append("\n");
        }



//        for (int i = 0; i < h; i++) {
//            int a = (h - 1) - i;
//            for (int b = a; b > 0; b--) {
//                sb.append(" ");
//            }
//            int c = (i * 2) + 1;
//            for (int d = c; d > 0; d--) {
//                sb.append("^");
//            }
////            for (int b = a; b > 0; b--) {
////                sb.append(" ");
////            }
//            sb.append("\n");
//        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(new Paint().piramid(10));
    }
}

