package ru.bildovich;

/**
 * Created by mac on 04.02.17.
 */
public class Counter {
    int result;
    public int add(int start, int finish){
        result = 0;
        for (int i=start; i < finish ; i++) {
            if((i%2)==0){
                result=result+i;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int r = new Counter().add(1, 5);
        System.out.println(r);
    }
}
