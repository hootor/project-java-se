/**
 * Created by mac on 10.02.17.
 */
public class Main {

    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7,8,9};
        arr = reverse(arr);

        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
    public static int[] reverse(int arr[]){
        int j = arr.length-1;
        for (int i=0; i < j; i++, j--) {
            int buff = arr[i];
            arr[i] = arr[j];
            arr[j] = buff;
        }
        return arr;
    }


}
