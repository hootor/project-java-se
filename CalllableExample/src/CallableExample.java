import java.util.Date;
import java.util.concurrent.*;

/**
 * Created by mac on 28.09.17.
 * Class
 */
public class CallableExample {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

        Callable c = new Callable<String>() {
            @Override
            public String call() throws Exception {
                System.out.println("Inside 'call' 1");
                Thread.sleep(1000);
                System.out.println("Inside 'call' 2");
                return "Hello world!";
            }
        };

        ExecutorService es = Executors.newSingleThreadExecutor();
        Future<String> answer = es.submit(c);
        System.out.println("After call 1 : " + new Date(System.currentTimeMillis()));
        Thread.sleep(2000);

        System.out.println("Is done 1 : " + answer.isDone());

        String hello = answer.get();
        System.out.println("After call 2 : " + new Date(System.currentTimeMillis()));
        System.out.println("Is done 2 : " + answer.isDone());
        System.out.println();
        System.out.println("Answer: " + hello);

        es.shutdown();
        }
}

