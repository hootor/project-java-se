package ru.bildovich;

import oracle.jvm.hotspot.jfr.JFR;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by mac on 13.09.17.
 * Class
 */
public class Form extends JFrame {

    public Form() throws HeadlessException {

        JButton btn = new JButton("PRESS ME");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Кнопка нажата");
            }
        });
        add(btn);

    }

    public static void main(String[] args) {

        Form f = new Form();

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setBounds(100, 100, 200, 200);
        f.setVisible(true);
    }
}
