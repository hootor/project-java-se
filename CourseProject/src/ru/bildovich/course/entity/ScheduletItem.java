package ru.bildovich.course.entity;

/**
 * Created by mac on 13.09.17.
 * Class
 */
public class ScheduletItem {
    private int dayOfWeek;
    private String startTime;
    private String endTime;

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}


enum DayOfWeeks{
        MONDAY, TUESTDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
}
