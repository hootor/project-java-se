package com.bildovich.consumer;

//import mti.password.MTIPasswordGenerator;
import ru.bildovich.PasswordGenerator;

/**
 * Created by mac on 16.09.17.
 * Class
 */
public class PasswordConsumer {
    public static void main(String[] args) {
        PasswordGenerator pg = creatPasswordGenerator("mti.password.MTIPasswordGenerator");
        System.out.println(pg.generatePassword());
    }

    private static PasswordGenerator creatPasswordGenerator(String s) {
        try {
            Class clazz = Class.forName(s);
            return (PasswordGenerator) clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
