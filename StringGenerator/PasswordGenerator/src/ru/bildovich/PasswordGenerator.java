package ru.bildovich;

/**
 * Created by mac on 16.09.17.
 * Class
 */
public interface PasswordGenerator {

    public String generatePassword();

}
