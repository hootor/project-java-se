package ru.bildovich.book.exception;

/**
 * Created by mac on 13.09.17.
 * Class
 */
public class BookDAOException extends BookException {

    public BookDAOException() {
    }

    public BookDAOException(String message) {
        super(message);
    }

    public BookDAOException(String message, Throwable cause) {
        super(message, cause);
    }
}
