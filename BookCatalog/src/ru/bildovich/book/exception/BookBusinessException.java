package ru.bildovich.book.exception;

/**
 * Created by mac on 13.09.17.
 * Class
 */
public class BookBusinessException extends BookException {

    public BookBusinessException() {
    }

    public BookBusinessException(String message) {
        super(message);
    }

    public BookBusinessException(Throwable cause) {
        super(cause);
    }

    public BookBusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
