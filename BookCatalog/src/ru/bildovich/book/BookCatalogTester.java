package ru.bildovich.book;

import ru.bildovich.book.business.BookCatalog;
import ru.bildovich.book.domain.Book;

/**
 * Created by mac on 13.09.17.!_________----------
 * Class
 */
public class BookCatalogTester {
    public static void main(String[] args) {
        BookCatalog catalog = new BookCatalog();

        Book b = new Book();
        catalog.addBook(b);
        catalog.updateBook(b);
        System.out.println(catalog.getBook(b.getBookId()));
        catalog.deleteBook(b);

    }
}
