package ru.bildovich.book.business;

import ru.bildovich.book.dao.BookDAO;
import ru.bildovich.book.dao.BookDAOFactory;
import ru.bildovich.book.domain.Book;
import ru.bildovich.book.exception.BookBusinessException;
import ru.bildovich.book.exception.BookDAOException;
import ru.bildovich.book.exception.BookException;

public class BookCatalog {

    private BookDAO dao = BookDAOFactory.getDAO();

    public Long addBook(Book book) throws BookBusinessException{
        try {
            return dao.addBook(book);
        } catch (BookException e) {
            throw new BookBusinessException(e);
        }
    }

    public void updateBook(Book book) throws BookBusinessException {
        try {
            dao.updateBook(book);
        } catch (BookDAOException e) {
            throw new BookBusinessException(e);
        }
    }

    public void deleteBook(Book book)throws BookBusinessException{
        try {
            dao.deleteBook(book);
        } catch (BookDAOException e) {
            throw new BookBusinessException(e);
        }
    }

    public Book getBook(Long bookId)throws BookBusinessException{
        try {
            return dao.getBook(bookId);
        } catch (BookDAOException e) {
            throw new BookBusinessException(e);
        }
    }
}
