package ru.bildovich.book.dao;

import ru.bildovich.book.domain.Book;
import ru.bildovich.book.exception.BookDAOException;
import ru.bildovich.book.exception.BookException;

/**
 * Created by mac on 13.09.17.
 * Class
 */
public class BookFakeDAO implements BookDAO {

    @Override
    public Long addBook(Book book) throws BookException {

        try {
            System.out.println("Add book for fake");
        } catch (Exception ex) {
            throw new BookException(ex);
        }
        return 1L;
    }

    @Override
    public void updateBook(Book book) throws BookDAOException {
        System.out.println("Update book for fake");
    }

    @Override
    public void deleteBook(Book book) throws BookDAOException {
        System.out.println("Delete book for fake");
    }

    @Override
    public Book getBook(Long bookId) throws BookDAOException {
        return new Book(99L, "ISBNTEST", "TEST BOOK", 199.01);
    }
}
