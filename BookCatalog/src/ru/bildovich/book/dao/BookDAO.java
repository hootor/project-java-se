package ru.bildovich.book.dao;

import ru.bildovich.book.domain.Book;
import ru.bildovich.book.exception.BookDAOException;
import ru.bildovich.book.exception.BookException;

/**
 * Created by mac on 13.09.17.
 * Class
 */
public interface BookDAO {

    public Long addBook(Book book) throws BookException;

    public void updateBook(Book book) throws BookDAOException;

    public void deleteBook(Book book) throws BookDAOException;

    public Book getBook(Long bookId) throws BookDAOException;
}
