package ru.bildovich.book.domain;

/**
 * Created by mac on 12.09.17.
 * Class
 */
public class Book {

    private Long bookId;
    private String title;
    private Double price;
    private String isba;

    public Book() {
    }

    public Book(Long bookId, String isba, String title, Double price) {
        this.bookId = bookId;
        this.title = title;
        this.price = price;
        this.isba = isba;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getIsba() {
        return isba;
    }

    public void setIsba(String isba) {
        this.isba = isba;
    }
}
