//
//  GBOrange.h
//  Lesson 3
//
//  Created by Мас on 13.02.17.
//  Copyright © 2017 Мас. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBOrange : NSObject

{
    @public;
    NSString *color;
    NSString *taste;
    int radius;
}

- (void) orangeVolume;

@end
