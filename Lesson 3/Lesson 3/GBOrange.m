//
//  GBOrange.m
//  Lesson 3
//
//  Created by Мас on 13.02.17.
//  Copyright © 2017 Мас. All rights reserved.
//

#import "GBOrange.h"

@implementation GBOrange

- (void) orangeVolume
{
    float orangeVolume = (4/3) * M_PI * pow(self->radius, 3);
    NSLog(@"orangeVolume %f", orangeVolume);
}
@end
