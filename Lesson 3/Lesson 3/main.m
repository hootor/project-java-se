//
//  main.m
//  Lesson 3
//
//  Created by Мас on 01.02.17.
//  Copyright © 2017 Мас. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Human.h"
#import "GBOrange.h"

typedef struct{
    int year;
    int countDoor;
    NSString *name;
} Car;

typedef struct{
    int r;
    int g;
    int b;
    int a;
} ColorRGBA;

void logCar(Car car){
    NSLog(@"name %@ year %d door %d", car.name, car.year, car.countDoor);
}

void addNumber(int *a){
    *a = *a + 1;
}

void swap(int **value1, int **value2){
    int *temp = *value1;
    *value1 = *value2;
    *value2 = temp;
    
}

int main(int argc, const char * argv[]) {
   
    
    Car myCar;
    myCar.year = 2009;
    myCar.countDoor = 4;
    myCar.name = @"BBMo";
    
    Car newCar = {2017, 5, @"NIVA"};
    Car *carcar = &newCar;
    (*carcar).name = @"ZP";
    
    myCar = newCar;
    logCar(myCar);
    
    int a = 10;
    NSLog(@"a - %d", a);
    
    int *r = &a;
    
    a = 100;
    NSLog(@"r - %d", *r);
    
    *r = 200;
    
    NSLog(@"a - %d", a);
    
    
    int *g = NULL;
    NSLog(@"%p", g);
    if (g==NULL) {
        NSLog(@"no");
    }else{
        NSLog(@"Yes");
        
    }
    
    int i = 100;
    addNumber(&i);
    NSLog(@"%d", i);
    
    int r1 = 100;
    int r2 = 200;
    
    int *g1= &r1;
    int *g2 = &r2;
    
    NSLog(@"g1 %d g2 %d", *g1, *g2);
    swap(&g1, &g2);
//    NSLog(@"g1 %d g2 %d", *g1, *g2);
    
//    NSString *text = [[NSString alloc]init];
//    int *ref = malloc(<#size_t __size#>);
//    
//    free(ref);
    
    
    Human *human = [[Human alloc]init];
    human->name = @"Kot";
    human->money = 11111;
    human->age = 5;
    
    NSLog(@"%@", (*human).name);
    
    
    
    GBOrange *sameOrange = [[GBOrange alloc]init];
    sameOrange->color = @"Red";
    sameOrange->taste = @"Sweet";
    sameOrange->radius = 20;
    NSLog(@"Orange has %@ color and %@ taster", sameOrange->color, sameOrange->taste);
    CGFloat orangeVolume = (4/3)*M_PI*pow(sameOrange->radius, 3);
    NSLog(@"orangeVolume %f", orangeVolume);
    [sameOrange orangeVolume];
    [sameOrange release];
    
    
    
    return 0;
}
