//
//  Human.h
//  Lesson 3
//
//  Created by Мас on 05.02.17.
//  Copyright © 2017 Мас. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Human : NSObject
{
    @public
    
    NSString *name;
    int age;
    int money;
}
@end
